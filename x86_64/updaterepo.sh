#!/usr/bin/env bash

#repo-add op-arch-repo.db.tar.gz *.pkg.tar.zst

#/usr/bin/rm op-arch-repo.db
#/usr/bin/rm op-arch-repo.files

#/usr/bin/mv op-arch-repo.db.tar.gz op-arch-repo.db
#/usr/bin/mv op-arch-repo.files.tar.gz op-arch-repo.files

repo-add -n -R op-arch-repo.db.tar.gz *.pkg.tar.zst

rm op-arch-repo.db
#rm op-arch-repo.db.sig
rm op-arch-repo.files
#rm op-arch-repo.files.sig

mv op-arch-repo.db.tar.gz op-arch-repo.db
#mv op-arch-repo.db.tar.gz.sig op-arch-repo.sig
mv op-arch-repo.files.tar.gz op-arch-repo.files
#mv op-arch-repo.files.tar.gz.sig op-arch-repo.files.sig
