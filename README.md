# op-arch-repo
My Arch Linux Repository where i put my packages and AUR Packages that [op-script](https://gitlab.com/orasponka/op-script) uses.

## How to enable this repository?
Either run the commands below
```
$ wget https://gitlab.com/orasponka/op-arch-repo/-/raw/main/enable
$ chmod +x enable
$ ./enable
```
Or add the text below to your /etc/pacman.conf file
```
[op-arch-repo]
SigLevel = Optional DatabaseOptional
Server = https://gitlab.com/orasponka/$repo/-/raw/main/$arch
```
